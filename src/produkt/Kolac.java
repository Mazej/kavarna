// package kavarna;
package produkt;

import java.util.HashMap;

/**
 * @author Matěj Kubík
 */
public class Kolac extends Produkt {

    public Kolac(String nazev, float cena, String[] suroviny, int[] mnozstvi) {
        super(nazev, cena, suroviny, mnozstvi);
    }

    @Override
    public Timer vyrob(int pocet) {
        return new Timer(1800 * (pocet % 4), this.nazev);
    }
}
