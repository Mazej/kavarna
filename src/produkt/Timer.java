package produkt;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class Timer extends Pane {
    private Timeline animation;
    private int tmp;
    private String S = "";
    private Label label;
    private Label nazev;

    public Timer(int tmp, String nazev) {
        this.tmp = tmp;
        label = new Label(String.valueOf(tmp));
        label.setFont(javafx.scene.text.Font.font(100));
        label.setTranslateX(250);
        label.setTranslateY(400);
        this.nazev = new Label(nazev);


        getChildren().addAll(this.nazev, label);
        animation = new Timeline(new KeyFrame(Duration.seconds(1), e -> timelabel()));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.play();
    }

    private void timelabel() {
        if (tmp > 0) tmp--;
        S = tmp + "";
        label.setText(S);
    }
}
