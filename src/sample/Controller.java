package sample;

import exceptions.NotEnoughException;
import exceptions.NotFoundException;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import kavarna.Kavarna;
import vyroba.VyrobaControll;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller extends Application implements Initializable {
    @FXML
    private ListView<String> lSouhrn;
    @FXML
    private ListView<String> lvKava;
    @FXML
    private ListView<String> lvKolace;
    @FXML
    private VBox vbPlus;
    @FXML
    private VBox vbMinus;
    @FXML
    private Label lSellResult;
    @FXML
    private ListView<Integer> lvSouhrnPocet;
    @FXML
    private Label lbSklad;
    @FXML
    private Label lbPokladna;

    private Kavarna k = new Kavarna();


    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Stage window = new Stage();
        window.setTitle("Kavarna");
        window.setScene(new Scene(root, 800, 700));
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        k.load();
//        k.testovaciHodnoty();
//        k.save();

        pridavejDoSouhrn(lvKava);
        pridavejDoSouhrn(lvKolace);
        ObservableList<String> oKava = FXCollections.observableArrayList();
        ObservableList<String> oKolace = FXCollections.observableArrayList();
        k.naplnitNabidku(oKava, oKolace);
        lvKava.setItems(oKava);
        lvKolace.setItems(oKolace);
        lbPokladna.setText(k.vypisPokladnu());
        lvSouhrnPocet.getItems().addListener((ListChangeListener<Integer>) change -> vypocitejCenu()); // provadi vypocet ceny pri zmene Souhrnu
    }

    // presun produktu do souhrnu po kliknuti v nabidce
    private void pridavejDoSouhrn(ListView<String> listView) {
        listView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> ov, String old_val, String new_val) -> {
            String selectedItem = new_val;
            if (lSouhrn.getItems().contains(selectedItem)) {
                int index = lSouhrn.getItems().indexOf(selectedItem);
                lvSouhrnPocet.getItems().set(index, lvSouhrnPocet.getItems().get(index) + 1);
                return;
            }
            lSouhrn.getItems().add(selectedItem);
            Button plus = new Button("+");
            Button minus = new Button("-");
            plus.setOnMouseClicked(e -> {
                int newIndex = ((VBox) ((Button) e.getSource()).getParent()).getChildren().indexOf(e.getSource());
                lvSouhrnPocet.getItems().set(newIndex, lvSouhrnPocet.getItems().get(newIndex) + 1);
            });
            minus.setOnMouseClicked(e -> {
                int newIndex = ((VBox) ((Button) e.getSource()).getParent()).getChildren().indexOf(e.getSource());
                int pocet = lvSouhrnPocet.getItems().get(lSouhrn.getItems().indexOf(selectedItem));
                if (pocet < 2) {    // mazani radku pokud je pocet mensi, jak 1
                    lSouhrn.getItems().remove(newIndex);
                    lvSouhrnPocet.getItems().remove(newIndex);
                    vbPlus.getChildren().remove(newIndex);
                    vbMinus.getChildren().remove(newIndex);
                    return;
                }
                lvSouhrnPocet.getItems().set(lSouhrn.getItems().indexOf(selectedItem), pocet - 1);
            });
            vbPlus.getChildren().add(plus);
            vbMinus.getChildren().add(minus);
            lvSouhrnPocet.getItems().add(1);
        });
    }

    @FXML
    private void vypocitejCenu() {
        String output;
        try {
            float cena = 0;
            for (int i = 0; i < lSouhrn.getItems().size(); i++) {
                cena += k.vypocetCeny(lSouhrn.getItems().get(i), lvSouhrnPocet.getItems().get(i));
            }
            output = "Vysledna cena: " + cena;
        } catch (NotEnoughException nee) {
            output = String.format("Nedostatek %s", nee.getWhat());
        } catch (NotFoundException nfe) {
            output = "Nenalezeno" + nfe.getWhat();
        } catch (Exception e) {
            e.printStackTrace();
            output = "Neznama chyba";
        }
        this.lSellResult.setText(output);
    }

    @FXML
    public void zrusitObjednavku() {
        lSouhrn.getItems().clear();
        lvSouhrnPocet.getItems().clear();
        vbPlus.getChildren().clear();
        vbMinus.getChildren().clear();
        lSellResult.setText("");
    }

    @FXML
    public void handleBtnSklad() {
        if (lbSklad.getText() != "")
            lbSklad.setText("");
        else lbSklad.setText(k.vypisSklad());

    }

    public void handleBtnPotvrditProdej() {
        String output;
        try {
            for (int i = 0; i < lSouhrn.getItems().size(); i++) {
                k.prodej(lSouhrn.getItems().get(i), lvSouhrnPocet.getItems().get(i));
            }
            this.zrusitObjednavku();
            output = "";
        } catch (NotEnoughException nee) {
            output = String.format("Nedostatek %s", nee.getWhat());
        } catch (NotFoundException nfe) {
            output = "Nenalezeno" + nfe.getWhat();
        } catch (Exception e) {
            e.printStackTrace();
            output = "Neznama chyba";
        }
        this.lSellResult.setText(output);
        lbPokladna.setText(k.vypisPokladnu());
        k.save();
    }

    public void handleBtnVyrob() {
        VyrobaControll vyrobaControll = new VyrobaControll();
        if (vyrobaControll.display()) System.out.println("vyrobena");
        k.vyrob(vyrobaControll.getKolac(), vyrobaControll.getPocet());
    }
}










