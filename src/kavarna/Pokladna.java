package kavarna;

import java.io.Serializable;

/**
 * @author Matěj Kubík
 */
class Pokladna implements Serializable {
    private float zustatek;

    public Pokladna(float zustatek) {
        this.zustatek = zustatek;
    }

    public float getZustatek() {
        return this.zustatek;
    }

    public boolean pridej(float kolik) {
        this.zustatek += kolik;
        return true;
    }

    public boolean vyber(float kolik) {
        this.zustatek -= kolik;
        return true;
    }

    @Override
    public String toString() {
        return String.format("Pokladna: %f Kc", this.zustatek);
    }
};