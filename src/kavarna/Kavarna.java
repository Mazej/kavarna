package kavarna;

import exceptions.NotEnoughException;
import exceptions.NotFoundException;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import produkt.Kava;
import produkt.Kolac;
import seznam.Nabidka;
import seznam.Sklad;

import java.io.*;
import java.util.HashMap;

/**
 * @author Matěj Kubík
 */
public class Kavarna {

    private Pokladna pokladna = new Pokladna(500);
    private Nabidka<Kava> nKava = new Nabidka<Kava>("nKava");// nabidka kavy
    private Nabidka<Kolac> nKolace = new Nabidka<Kolac>("nKolace");// nabidka kolacu
    private Sklad sklad = new Sklad("sklad"); // sklad se Surovinami


    /**
     * Prace se SKLADEM
     */
    public void pridejSurovinu(String nazev, int kolik) {
        sklad.pridej(nazev, kolik);
    }

    public void pridejSurovinu(String nazev) {
        sklad.pridej(nazev, 0);
    }

    public void pridejSurovinu(String[] nazvy, int[] kolik) {
        for (int i = 0; i < nazvy.length; i++)
            sklad.pridej(nazvy[i], kolik[i]);
    }

    public boolean odeberSurovinu(String nazev, int mnozstvi) {
        return (sklad.odeber(nazev, mnozstvi));
    }

    public void odeberSurovinu(String nazev) {
        sklad.odeber(nazev);
    }


    /**
     * prace s NABIDKOU
     */
    public void pridejProdukt(String zarazeni, String nazev, float cena, String[] ingredience, int[] mnozstvi) {
        if (zarazeni == "kava")
            this.nKava.pridej(new Kava(nazev, cena, ingredience, mnozstvi));
        else if (zarazeni == "kolac")
            this.nKolace.pridej(new Kolac(nazev, cena, ingredience, mnozstvi));
    }

    public void odeberProdukt(String nazev) {
        this.nKava.odeber(nazev);
        this.nKolace.odeber(nazev);
    }


    /**
     * PRODEJ a VYROBA
     */
    public float vypocetCeny(String co, int kolik) throws NotFoundException, NotEnoughException {
        float cena = 0;
        if (this.nKolace.get(co) == null && this.nKava.get(co) == null)
            throw new NotFoundException(co);
        else if (this.nKolace.get(co) != null) {
            if (!this.sklad.dostupnost(co, kolik))
                throw new NotEnoughException(co);
            cena = this.nKolace.get(co).getCena();
        } else {
            HashMap<String, Integer> ingred = new HashMap<>();
            for (String sur : this.nKava.get(co).ingredience().keySet())
                ingred.put(sur, this.nKava.get(co).ingredience().get(sur) * kolik);
            String vysl = sklad.dostupnost(ingred);
            if (!vysl.equals(""))
                throw new NotEnoughException(vysl);
            cena = this.nKava.get(co).getCena();
        }
        return cena * kolik;
    }

    public Node vyrob(String nazev, int pocet) {
        HashMap<String, Integer> ingred = new HashMap<>();
        for (String sur : this.nKolace.get(nazev).ingredience().keySet())
            ingred.put(sur, this.nKolace.get(nazev).ingredience().get(sur) * pocet);
        String vysl = this.sklad.odeber(ingred);
        if (!vysl.equals(""))
            return new TextField(vysl);
        this.sklad.pridej(nazev, pocet);
        return this.nKolace.get(nazev).vyrob(pocet);
    }

    public void prodej(String co, int kolik) throws Exception {
        float cena = 0;
        if (this.nKolace.get(co) == null && this.nKava.get(co) == null)
            throw new NotFoundException(co);
        else if (this.nKolace.get(co) != null) {
            if (!this.sklad.odeber(co, kolik))
                throw new NotEnoughException(co);
            cena = this.nKolace.get(co).getCena();
        } else {
            HashMap<String, Integer> ingred = new HashMap<>();
            for (String sur : this.nKava.get(co).ingredience().keySet())
                ingred.put(sur, this.nKava.get(co).ingredience().get(sur) * kolik);
            String vysl = sklad.odeber(ingred);
            if (!vysl.equals(""))
                throw new NotEnoughException(vysl);
            cena = this.nKava.get(co).getCena();
        }
        this.pokladna.pridej(cena * kolik);
    }


    /**
     * UKLADANI a NACITANI
     */
    public void save() {
        try {
            sklad.save();
            nKava.save();
            nKolace.save();
            FileOutputStream fos = new FileOutputStream("pokladna.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this.pokladna);
            oos.close();
            fos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


    public void load() {
        try {
            sklad.load();
            nKava.load();
            nKolace.load();
            FileInputStream fis = new FileInputStream("pokladna.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.pokladna = (Pokladna) ois.readObject();
            ois.close();
            fis.close();
        } catch (Exception c) {
            testovaciHodnoty();
        }
    }

        public void testovaciHodnoty(){
            this.pridejSurovinu(new String[]{"mleko", "mouka", "cukr", "maslo", "olej", "cukr", "mak", "espresso", "repa", "cokolada", "makovak", "repak"},
                    new int[]{5000, 6000, 3000, 4000, 1000, 2000, 500, 1000, 300, 500, 5, 10});
            this.pridejProdukt("kava", "espresso", 30.50f, new String[]{"espresso"}, new int[]{30});
            this.pridejProdukt("kava", "late", 50, new String[]{"espresso", "mleko"}, new int[]{30, 20});
            this.pridejProdukt("kava", "cappuccino", 50, new String[]{"espresso", "mleko"}, new int[]{30, 50});
            this.pridejProdukt("kolac", "makovak", 45, new String[]{"mouka", "olej", "cukr", "mak"},
                    new int[]{50, 10, 15, 20});
            this.pridejProdukt("kolac", "repak", 45, new String[]{"mouka", "olej", "cukr", "repa"},
                    new int[]{50, 10, 15, 30});
            this.pokladna = new Pokladna(500);
        }



    /**
     * VYPISY
     */
    public void naplnitNabidku(ObservableList<String> kava, ObservableList<String> kolace) {
        this.nKava.observable(kava);
        this.nKolace.observable(kolace);
    }

    public String vypisIngredience() {
        return this.nKolace.vypisIngredience() + this.nKava.vypisIngredience();
    }

    public String vypisSklad() {
        return this.sklad.toString();
    }

    public String vypisNabidku() {
        return this.vypisNabidku("kava") + this.vypisNabidku("kolace");
    }

    public String vypisPokladnu() {
        return this.pokladna.toString();
    }


    public String vypisNabidku(String nabidka) {
        if (nabidka == "kava")
            return "\n\nKAVA\n\n" + this.nKava.toString();
        else if (nabidka == "kolace")
            return "\n\nKOLACE\n\n" + this.nKolace.toString();
        return "Spatne zadany nazev nabidky...";
    }
}
