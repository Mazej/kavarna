package vyroba;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.stage.Stage;

public class VyrobaControll {

    @FXML
    private RadioButton makovak;
    @FXML
    private RadioButton mrkvak;
    @FXML
    private Button zrusit;
    @FXML
    private Spinner<Integer> pocitadlo;
    @FXML
    private Button vyrobit;

    private int pocet;
    private String kolac;
    private boolean odpoved;
    private Stage window;

    public boolean display() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("vyroba.fxml"));
            Parent root1 = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(this.zrusit);
        SpinnerValueFactory<Integer> pocitadloValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 20, 4);
        this.pocitadlo.setValueFactory(pocitadloValueFactory);
        this.pocitadlo.setEditable(true);
        this.zrusit.setOnAction(e -> {
            window.close();
            this.odpoved = false;
        });
        this.vyrobit.setOnAction(e -> {
            if (makovak.isSelected()) this.kolac = "makovak";
            if (mrkvak.isSelected()) this.kolac = "mrkvak";
            this.pocet = this.pocitadlo.getValue();
        });
        return this.odpoved;
    }

    public int getPocet() {
        return this.pocet;
    }

    public String getKolac() {
        return this.kolac;
    }
}






