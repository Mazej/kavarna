package seznam;

import javafx.collections.ObservableList;

import java.io.*;
import java.util.HashMap;

public abstract class Seznam<H> implements Serializable {
    protected HashMap<String, H> s;
    protected String name;

    public Seznam(String name) {
        this.name = name;
        this.s = new HashMap<>();
    }

    public boolean odeber(String nazev) {
        this.s.remove(nazev);
        return true;
    }

    public H get(String nazev) {
        return this.s.get(nazev);
    }

    public void save() {
        try {
            FileOutputStream fos = new FileOutputStream(this.name + ".ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this.s);
            oos.close();
            fos.close();
            return;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return;
        }
    }

    public void load() throws IOException, ClassNotFoundException {
        try {
            FileInputStream fis = new FileInputStream(this.name + ".ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.s = (HashMap<String, H>) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException ioe) {
//            ioe.printStackTrace();
            throw ioe;
        } catch (ClassNotFoundException c) {
            throw new ClassNotFoundException();
        }
    }

    public ObservableList<String> observable(ObservableList<String> list) {
        for (String nazev : this.s.keySet())
            list.add(nazev);
        return list;
    }
}