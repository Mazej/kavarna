package seznam;

import produkt.Produkt;

public class Nabidka<T extends Produkt> extends Seznam<T> {

    public Nabidka(String name) {
        super(name);
    }

    public void pridej(T produkt) {
        this.s.put(produkt.getNazev(), produkt);
    }

    public void pridej(T[] produkty) {
        for (T produkt : produkty)
            this.pridej(produkt);
    }

    @Override
    public String toString() {
        String output = "NAZEV\tCENA";
        for (T produkt : this.s.values())
            output += produkt.toString();
        return output;
    }

    public String vypisIngredience() {
        String output = "";
        for (String produkt : this.s.keySet()) {
            output += "\n" + produkt + ": ";
            for (String surovina : this.s.get(produkt).ingredience().keySet())
                output += String.format("%s-%d, ", surovina, this.s.get(produkt).ingredience().get(surovina));
        }
        return output;
    }

}