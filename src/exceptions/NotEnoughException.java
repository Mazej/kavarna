package exceptions;

public class NotEnoughException extends Exception {
    protected String what;

    public NotEnoughException(String what) {
        this.what = what;
    }

    public String getWhat() {
        return this.what;
    }
}
